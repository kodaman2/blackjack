﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using System.Collections.Generic;
using Kodaman;

public class CardTests
{
	private int howManyCards = 20;

	[Test]
	public void NumberCardsTest ()
	{
		// 2 to 10
		List<Card> cards = new List<Card> ();

		for (int i = 0; i < howManyCards; i++) {
			cards.Add (new Card (Random.Range (2, 11), (SuitEnum)Random.Range (1, 5)));
			Debug.Log (cards [i].ToString ());
		}

		// Make sure cards ranks are between 2, and 10
		for (int i = 0; i < cards.Count; i++) {
			Assert.That (cards [i].getRank, Is.GreaterThanOrEqualTo (2));
			Assert.That (cards [i].getRank, Is.LessThanOrEqualTo (10));
		}
	}

	[Test]
	public void FaceCardsTest ()
	{
		List<FaceCard> cards = new List<FaceCard> ();

		for (int i = 0; i < howManyCards; i++) {
			cards.Add (new FaceCard (Random.Range (11, 14), (SuitEnum)Random.Range (1, 5)));
			Debug.Log (cards [i].ToString ());
		}

		// Make sure cards ranks are between 11, 13
		for (int i = 0; i < cards.Count; i++) {

			Assert.That (cards [i].getRank, Is.GreaterThanOrEqualTo (11));
			Assert.That (cards [i].getRank, Is.LessThanOrEqualTo (13));
			// Check soft and hard values
			Assert.That (cards [i].softValue, Is.EqualTo (10));
			Assert.That (cards [i].hardValue, Is.EqualTo (10));
		}

	}

	[Test]
	public void AceCardsTest ()
	{
		List<AceCard> cards = new List<AceCard> ();

		for (int i = 0; i < howManyCards; i++) {
			cards.Add (new AceCard (1, (SuitEnum)Random.Range (1, 5)));
			Debug.Log (cards [i].ToString ());
		}


		for (int i = 0; i < cards.Count; i++) {

			// Make sure cards ranks is 1
			Assert.That (cards [i].getRank, Is.EqualTo (1));
			// Check soft and hard values
			Assert.That (cards [i].softValue, Is.EqualTo (11));
			Assert.That (cards [i].hardValue, Is.EqualTo (1));
		}


	}
}