﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using System.Collections.Generic;
using Kodaman;

public class ShoeTests
{

	[Test]
	public void CreateShoe ()
	{
		int decks = 8;
		int stopDeal = 2;
		Shoe myShoe = new Shoe (decks, stopDeal);

		// 8 decks X 52 = 416
		Assert.That (myShoe.shuffledCards.Count, Is.EqualTo (decks * 52));
		Assert.That (myShoe.shuffledCards.Count, Is.EqualTo (decks * 52));

		myShoe.shuffle ();

		// Assert number of cards remaining
		Assert.That (myShoe.shuffledCards.Count, Is.EqualTo (stopDeal * 52));

	}

	[Test]
	public void CreateShoeFixedSeed ()
	{
		int decks = 8;
		int stopDeal = 3;
		int fixedSeed = 45;
		Shoe myShoe = new Shoe (decks, stopDeal, fixedSeed);

		myShoe.shuffle ();

		// Assert number of cards remaining
		Assert.That (myShoe.shuffledCards.Count, Is.EqualTo (stopDeal * 52));
	}
}
