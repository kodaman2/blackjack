﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using System.Collections.Generic;
using Kodaman;

public class HandTotalTests
{
	[Test]
	public void BlackjackTest01 ()
	{
		// Create a hand
		Hand myhand = new Hand ();

		// Add cards
		myhand.add (new Card (10, SuitEnum.Clubs));
		myhand.add (new AceCard (1, SuitEnum.Diamonds));
		myhand.value ();

		// Soft value of Ace is 11, so 11 plus 10 should give is 21.
		// Alt value will use Ace as 1, so alt value should be 11.
		Debug.Log (myhand.HandValue);
		Debug.Log (myhand.AltValue);

		Assert.AreEqual (21, myhand.HandValue);
		Assert.AreEqual (11, myhand.AltValue);

		// assert for a blackjack
		Assert.IsTrue (myhand.blackjack ());
		Assert.IsFalse (myhand.busted ());

	}

	[Test]
	public void BlackjackTest02 ()
	{
		Hand myhand = new Hand ();
		myhand.add (new AceCard (1, SuitEnum.Clubs));
		myhand.add (new FaceCard (11, SuitEnum.Hearts));
		myhand.value ();

		Assert.AreEqual (21, myhand.HandValue);
		Assert.AreEqual (11, myhand.AltValue);
		Assert.IsTrue (myhand.blackjack ());
		Assert.IsFalse (myhand.busted ());
	}

	[Test]
	public void AceTest01 ()
	{
		Hand myhand = new Hand ();
		myhand.add (new AceCard (1, SuitEnum.Clubs));
		myhand.add (new Card (2, SuitEnum.Spades));
		myhand.value ();

		Debug.Log (myhand.HandValue);
		Debug.Log (myhand.AltValue);

		Assert.AreEqual (13, myhand.HandValue);
		Assert.AreEqual (3, myhand.AltValue);

		// Test for busted
		Assert.IsFalse (myhand.busted ());
	}

	[Test]
	public void AceTest02 ()
	{
		Hand myhand = new Hand ();
		myhand.add (new AceCard (1, SuitEnum.Clubs));
		myhand.add (new Card (4, SuitEnum.Spades));
		myhand.value ();

		Debug.Log (myhand.HandValue);
		Debug.Log (myhand.AltValue);

		Assert.AreEqual (15, myhand.HandValue);
		Assert.AreEqual (5, myhand.AltValue);

		// Test for busted
		Assert.IsFalse (myhand.busted ());
	}

	[Test]
	public void AceTest03 ()
	{
		Hand myhand = new Hand ();
		myhand.add (new AceCard (1, SuitEnum.Clubs));
		myhand.add (new Card (6, SuitEnum.Spades));
		myhand.value ();

		Debug.Log (myhand.HandValue);
		Debug.Log (myhand.AltValue);

		Assert.AreEqual (17, myhand.HandValue);
		Assert.AreEqual (7, myhand.AltValue);

		// Test for busted
		Assert.IsFalse (myhand.busted ());
	}

	[Test]
	public void TwoAceTest01 ()
	{
		Hand myhand = new Hand ();
		myhand.add (new AceCard (1, SuitEnum.Clubs));
		myhand.add (new AceCard (1, SuitEnum.Diamonds));
		myhand.add (new FaceCard (11, SuitEnum.Hearts));
		myhand.value ();

		// hand value should be 11 + 1 + 10 = 22
		Debug.Log (myhand.HandValue);
		// alt value should be 1 + 1 + 10 = 12
		Debug.Log (myhand.AltValue);

		Assert.AreEqual (22, myhand.HandValue); // handvalue should be 12, since 22 is over
		Assert.AreEqual (12, myhand.AltValue); // altvalue should be 0 since is over 22 anyways

		// Test for busted
		Assert.IsFalse (myhand.busted ());
	}

	[Test]
	public void TwoAceTest02 ()
	{
		Hand myhand = new Hand ();
		myhand.add (new AceCard (1, SuitEnum.Clubs));
		myhand.add (new AceCard (1, SuitEnum.Diamonds));
		myhand.value ();

		// hand value should be 11 + 1 = 12
		Debug.Log (myhand.HandValue);
		// alt value should be 1 + 1 = 2
		Debug.Log (myhand.AltValue);

		Assert.AreEqual (12, myhand.HandValue);
		Assert.AreEqual (2, myhand.AltValue);

		// Test for busted
		Assert.IsFalse (myhand.busted ());
	}

	[Test]
	public void TwoAceTest03 ()
	{
		Hand myhand = new Hand ();
		myhand.add (new AceCard (1, SuitEnum.Clubs));
		myhand.add (new AceCard (1, SuitEnum.Diamonds));
		myhand.add (new Card (10, SuitEnum.Clubs));
		myhand.add (new Card (10, SuitEnum.Spades));
		myhand.value ();

		// hand value should be 11 + 1 = 12
		Debug.Log (myhand.HandValue);
		// alt value should be 1 + 1 = 2
		Debug.Log (myhand.AltValue);

		Assert.AreEqual (32, myhand.HandValue);
		Assert.AreEqual (22, myhand.AltValue);

		// Test for busted
		Assert.IsTrue (myhand.busted ());
	}

	[Test]
	public void HardValueTest01 ()
	{
		Hand myhand = new Hand ();
		myhand.add (new Card (5, SuitEnum.Clubs));
		myhand.add (new Card (7, SuitEnum.Diamonds));
		myhand.add (new FaceCard (11, SuitEnum.Hearts));
		myhand.value ();

		Debug.Log (myhand.HandValue);
		Debug.Log (myhand.AltValue);

		Assert.AreEqual (22, myhand.HandValue);
		Assert.AreEqual (0, myhand.AltValue);

		Assert.IsTrue (myhand.busted ());
	}

	[Test]
	public void HardValueTest02 ()
	{
		Hand myhand = new Hand ();
		myhand.add (new Card (5, SuitEnum.Clubs));
		myhand.add (new Card (7, SuitEnum.Diamonds));
		myhand.value ();

		Debug.Log (myhand.HandValue);
		Debug.Log (myhand.AltValue);

		Assert.AreEqual (12, myhand.HandValue);
		Assert.AreEqual (0, myhand.AltValue);

		Assert.IsFalse (myhand.busted ());
	}

	[Test]
	public void HardValueTest03 ()
	{
		Hand myhand = new Hand ();
		myhand.add (new Card (5, SuitEnum.Clubs));
		myhand.add (new Card (7, SuitEnum.Diamonds));
		myhand.add (new Card (3, SuitEnum.Diamonds));
		myhand.value ();

		Debug.Log (myhand.HandValue);
		Debug.Log (myhand.AltValue);

		Assert.AreEqual (15, myhand.HandValue);
		Assert.AreEqual (0, myhand.AltValue);

		Assert.IsFalse (myhand.busted ());
	}

	[Test]
	public void HardValueTest04 ()
	{
		Hand myhand = new Hand ();
		myhand.add (new Card (5, SuitEnum.Clubs));
		myhand.add (new Card (7, SuitEnum.Diamonds));
		myhand.add (new Card (8, SuitEnum.Diamonds));
		myhand.value ();

		Debug.Log (myhand.HandValue);
		Debug.Log (myhand.AltValue);

		Assert.AreEqual (20, myhand.HandValue);
		Assert.AreEqual (0, myhand.AltValue);

		Assert.IsFalse (myhand.busted ());
	}

	[Test]
	public void HardValueTest05 ()
	{
		Hand myhand = new Hand ();
		myhand.add (new Card (9, SuitEnum.Clubs));
		myhand.add (new Card (7, SuitEnum.Diamonds));
		myhand.add (new Card (8, SuitEnum.Diamonds));
		myhand.value ();

		Debug.Log (myhand.HandValue);
		Debug.Log (myhand.AltValue);

		Assert.AreEqual (24, myhand.HandValue);
		Assert.AreEqual (0, myhand.AltValue);

		Assert.IsTrue (myhand.busted ());
	}
}
