﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using System.Collections.Generic;
using Kodaman;

public class DeckTests
{
	[Test]
	public void CreateDeck ()
	{
		int aceCardCount = 0;
		int faceCardCount = 0;
		int numberCardCount = 0;

		Deck myDeck = new Deck ();
		myDeck.getCards ();

		foreach (Card card in myDeck.getCards()) {
			Debug.Log (card.ToString ());

			if (card.getRank () == 1) {
				aceCardCount++;
			} else if (card.getRank () >= 11 && card.getRank () <= 13) {
				faceCardCount++;
			} else if (card.getRank () >= 2 && card.getRank () <= 10) {
				numberCardCount++;
			}
		}

		// What do we need to assert?
		// Make sure there are 52 cards
		Assert.That (myDeck.getCards ().Count, Is.EqualTo (52));
		// Make sure there are 4 Aces, rank 1
		Assert.That (aceCardCount, Is.EqualTo (4));
		// Make sure there are 12 FaceCards, point value 10
		Assert.That (faceCardCount, Is.EqualTo (12));
		// And 36 number Cards
		Assert.That (numberCardCount, Is.EqualTo (36));

	}

	[Test]
	public void SuitTest ()
	{
		int clubsCount = 0;
		int diamondsCount = 0;
		int heartsCount = 0;
		int spadesCount = 0;

		Deck myDeck = new Deck ();
		myDeck.getCards ();

		foreach (Card card in myDeck.getCards()) {
			Debug.Log (card.ToString ());

			if (card.getSuit () == SuitEnum.Clubs) {
				clubsCount++;
			} else if (card.getSuit () == SuitEnum.Diamonds) {
				diamondsCount++;
			} else if (card.getSuit () == SuitEnum.Hearts) {
				heartsCount++;
			} else if (card.getSuit () == SuitEnum.Spades) {
				spadesCount++;
			}

		}

		// Assert there are 13 cards on each suit
		Assert.That (clubsCount, Is.EqualTo (13));
		Assert.That (diamondsCount, Is.EqualTo (13));
		Assert.That (heartsCount, Is.EqualTo (13));
		Assert.That (spadesCount, Is.EqualTo (13));
	}
}
