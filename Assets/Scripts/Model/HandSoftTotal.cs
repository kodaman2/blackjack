﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kodaman
{
	public class HandSoftTotal : HandTotal
	{
		public HandSoftTotal (Hand hand) : base (hand)
		{
			base._hand = hand;
		}

		public override int total (Card card)
		{
			int total = 0;

			foreach (Card cards in base._hand.Cards) {
				total += cards.softValue ();
			}

			total -= card.softValue ();
			return total;
		}

		public override int total ()
		{
			int total = 0;

			foreach (Card cards in base._hand.Cards) {
				total += cards.softValue ();
			}
			return total;
		}
	}
}
