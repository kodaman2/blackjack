﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kodaman
{
	public enum SuitEnum
	{
		Clubs = 1,
		Diamonds = 2,
		Hearts = 3,
		Spades = 4
	}

	public class Card
	{
		protected SuitEnum _suit;
		// The suit of the card. 1 thru 4
		protected int _rank;

		// Constructor
		public Card (int rank, SuitEnum suit)
		{
			_rank = rank;
			_suit = suit;
		}

		public int getRank ()
		{
			return _rank;
		}

		public SuitEnum getSuit ()
		{
			return _suit;
		}

		public virtual int softValue ()
		{
			return _rank;
		}

		public virtual int hardValue ()
		{
			return _rank;
		}

		//		public virtual void setAltTotal (Hand hand)
		//		{
		//
		//		}

		public override string ToString ()
		{
			return "[Card] Rank: " + _rank + " ;Suit " + _suit;
		}
	}
}
