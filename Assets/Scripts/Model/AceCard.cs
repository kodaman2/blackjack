﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kodaman
{
	public class AceCard : Card
	{
		public AceCard (int rank, SuitEnum suit) : base (rank, suit)
		{
			base._rank = rank;
			base._suit = suit;
		}

		public override int softValue ()
		{
			return 11;
		}

		public override int hardValue ()
		{
			return 1;
		}

		public override string ToString ()
		{
			return "[AceCard] Rank: A" + " ;Suit " + base._suit;
		}
	}
}
