﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kodaman
{
	public class Deck
	{

		List<Card> cards = new List<Card> ();

		public Deck ()
		{
			// Itirate through suits, then ranks
			for (int i = 1; i <= 4; i++) {
				for (int j = 1; j <= 13; j++) {

					if (j == 1) {
						cards.Add (new AceCard (1, (SuitEnum)i));
					} else if (j >= 2 && j <= 10) {
						cards.Add (new Card (j, (SuitEnum)i));
					} else if (j >= 11 && j <= 13) {
						cards.Add (new FaceCard (j, (SuitEnum)i));
					}
				}
			}
		}

		public List<Card> getCards ()
		{
			return cards;
		}
	}
}
