﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kodaman
{
	public enum FaceCardEnum
	{
		Jack = 11,
		Queen = 12,
		King = 13
	}

	public class FaceCard : Card
	{

		private int pointValue = 10;

		public FaceCard (int rank, SuitEnum suit) : base (rank, suit)
		{
			base._rank = rank;
			base._suit = suit;
		}

		public override int softValue ()
		{
			return pointValue;
		}

		public override int hardValue ()
		{
			return pointValue;
		}

		public override string ToString ()
		{

			switch (base._rank) {
			case 11:
				return "[FaceCard] Rank: J(11)" + " ;Suit " + base._suit;
			case 12:
				return "[FaceCard] Rank: Q(12)" + " ;Suit " + base._suit;
			case 13:
				return "[FaceCard] Rank: K(13)" + " ;Suit " + base._suit;
			default:
				break;
			}

			return null;
		}

	}
}
