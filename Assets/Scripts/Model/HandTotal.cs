﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kodaman
{
	public abstract class HandTotal
	{

		protected int _total;
		protected Hand _hand;

		/// <summary>
		/// Creates a new HandTotal object associated with a given hand.
		/// </summary>
		/// <param name="hand">The hand for which to compute a total</param>
		public HandTotal (Hand hand)
		{
			_hand = hand;
		}

		/// <summary>
		/// Computes total of the cards in the hand except for the indicated card.
		/// This method is abstract. Each subclass must provide an implementation.
		/// </summary>
		/// <param name="card">A card to exclude from the total.</param>
		public abstract int total (Card card);


		/// <summary>
		/// Computes a total of all the cards in the associated hand.
		/// This method is abstract. Each subclass must provide an implementation.
		/// </summary>
		public abstract int total ();
	

	}
}
