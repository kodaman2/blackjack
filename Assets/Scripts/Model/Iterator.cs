﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Kodaman
{
	public class Iterator
	{
		private List<Card> _cards;
		private bool _isLastCard = false;

		public bool IsLastCard {
			get {
				return _isLastCard;
			}
		}

		// Enumerators are positioned before the first element
		// until the first MoveNext() call
		int position = -1;

		public Iterator (List<Card> cards)
		{
			_cards = cards;
		}

		public bool MoveNext ()
		{
			position++;

			if (position == _cards.Count - 2) {
				_isLastCard = true;
			}

			return (position < _cards.Count);
		}

		public void Reset ()
		{
			position = -1;
		}

		public Card Current {
			get {
				try {
					return _cards [position];
				} catch (IndexOutOfRangeException) {
					throw new InvalidOperationException ();
				}
			}
		}
	}
}
