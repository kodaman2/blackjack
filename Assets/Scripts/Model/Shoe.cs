﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kodaman
{
	public class Shoe
	{
	
		private List<Card> _unshuffledCards = new List<Card> ();
		private List<Card> _shuffledCards;
		private Card currentCard;
		private Iterator _iterator;

		Deck myDeck;

		private int _decks;
		private int _deal;
		private int _stopDeal;
		private System.Random _rng;
		private int _seed;
		private bool _isLastShoeCard = false;

		public Card CurrentCard {
			get {
				return currentCard;
			}
		}

		public bool isLastShoeCard {
			get {
				return _isLastShoeCard;
			}
		}

		public List<Card> unshuffledCards {
			get {
				return _unshuffledCards;
			}
		}

		public List<Card> shuffledCards {
			get {
				return _shuffledCards;
			}
		}

		//TODO merge somehow this two contructors to avoid code repetition
		/// <summary>
		/// Initializes a new instance of the <see cref="Shoe"/> class.
		/// </summary>
		/// <description>The Shoe creates a list of N decks. It also creates a new RNG with System.Random</description>
		/// <param name="decks">Number of Decks.</param>
		/// <param name="stopDeal">Stop deal. If the stop deal is 2, then it will leave two decks on the shoe.
		/// (Note: After shuffling has taken place)</param>
		public Shoe (int decks, int stopDeal)
		{
			_stopDeal = stopDeal;
			_deal = 0;
			_decks = decks;
			_isLastShoeCard = false;

			// Make a deck once
			myDeck = new Deck ();

			AddCardsToList (decks, myDeck, _unshuffledCards);
			_shuffledCards = new List<Card> (_unshuffledCards);
			_iterator = new Iterator (_shuffledCards);

			_rng = new System.Random ();


		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Shoe"/> class. (NB! Only use this for testing)
		/// </summary>
		/// <description>The Shoe creates a list of N decks. It also creates a new RNG with System.Random and
		/// a fixed seed for testing purposes. It will shuffle cards but in the same order, until the fixed
		/// seed is manually changed.</description>
		/// <param name="decks">Number of Decks.</param>
		/// <param name="stopDeal">Stop deal. If the stop deal is 2, then it will leave two decks on the shoe.
		/// (Note: After shuffling has taken place)</param>
		/// <param name="seed">A fixed seed used for testing purposes.</param>
		public Shoe (int decks, int stopDeal, int seed)
		{
			_stopDeal = stopDeal;
			_deal = 0;
			_decks = decks;
			_isLastShoeCard = false;

			// Make a deck once
			myDeck = new Deck ();

			AddCardsToList (decks, myDeck, _unshuffledCards);
			_shuffledCards = new List<Card> (_unshuffledCards);
			_iterator = new Iterator (_shuffledCards);

			_rng = new System.Random (seed);
		}

		/// <summary>
		/// Shuffles the shoe list, which is a list of N decks. Once shuffled it gets rid of N decks depending on stopDeal.
		/// The Shoe is then left with playable shuffled cards.
		/// </summary>
		/// <remarks>One thing to now, is that you can call shufffle as many times as you want.
		/// Even if you already have drawn cards from the deck.</remarks>
		public void shuffle ()
		{
			for (int i = 0; i < _shuffledCards.Count; i++) {

				int j = _rng.Next (_shuffledCards.Count);
				Card cardTemp = _shuffledCards [i];
				_shuffledCards [i] = _shuffledCards [j];
				_shuffledCards [j] = cardTemp;
			}
			Debug.Log ("Cards have been shuffled!");

			// Safety checks
			if (_stopDeal >= _decks) {
				Debug.LogError ("Stop deal cannot be larger or equal than decks number");
				return;
			}

			// Exlude decks only if both lists are the same Count
			// If stopDeal is non-zero, exclude several decks from the deal
			// 8 decks = 416, stopDeal = 2. Leave 104 cards in the show, and remove the rest.
			if (_shuffledCards.Count == _unshuffledCards.Count) {

				if (_stopDeal > 0) {

					int indexTemp = _stopDeal * 52;
					int countTemp = _shuffledCards.Count - indexTemp;

					_shuffledCards.RemoveRange (indexTemp - 1, countTemp);
				}
				Debug.Log ("Excess Decks have been removed from the List");
			}

		}

		/// <summary>
		/// Resets the shoe. Clears the shuffledCards list, and copies the unshuffledCards.
		/// Make sure to call shuffle method, after resetting the shoe.
		/// </summary>
		public void resetShoe ()
		{
			// TODO: Might need more testing on resetShoe()

			_deal = 0;
			_isLastShoeCard = false;

			// Clear lists, except _unshuffledCards since it stays the same
			_shuffledCards.Clear ();

			foreach (Card card in _unshuffledCards) {
				_shuffledCards.Add (card);
			}

			AddCardsToList (_decks, myDeck, _unshuffledCards);

		}

		/// <summary>
		/// Resets the shoe and shuffle.
		/// </summary>
		public void resetShoeAndShuffle ()
		{
			resetShoe ();
			shuffle ();
		}

		/// <summary>
		/// Deals the next card from the list. For gameplay make sure isTest is false.
		/// </summary>
		/// <description>Next card is pulled from the list. If isTest is true, it will pull cards
		/// from the unshuffledCards, helpful in testing phases. Otherwise for real gameplay
		/// make sure is set to false to pull cards from shuffledCards list.</description>
		/// <param name="isTest">Indicates whether this is a test.</param>
		public void dealCard (bool isTest)
		{
//			if (isTest) {
//				iterator (_unshuffledCards);
//			} else
//				iterator (_shuffledCards);
		}

		/// <summary>
		/// Iterates through a list of Cards, also sets isLastShoeCard variable.
		/// </summary>
		/// <param name="cards">A List of Type Card</param>
		/// <returns>Returns one card</returns>
		public void nextCard ()
		{
			// TODO: Refactor this method, should be void since we are using a property now!

			if (_isLastShoeCard) {
				Debug.Log ("There are no more cards, reset and reshuffle shoe!");
				return;
			}

			// If cards list is empty do nothing
			if (_shuffledCards == null) {
				Debug.Log ("Cards object is null!");
				return;
			}

			// Deal a card until is out
			if (_deal < _shuffledCards.Count) {
				//Debug.Log (_shuffledCards [_deal].ToString ());
				currentCard = _shuffledCards [_deal];
				_deal++;

				if (_deal == _shuffledCards.Count) {
					_isLastShoeCard = true;
					Debug.LogWarning ("Last card on the shoe");
				} else
					_isLastShoeCard = false;
			}
				
		}

		public void nextCardIterator ()
		{
			if (_iterator.IsLastCard) {
				_isLastShoeCard = true;
			}


			if (_iterator.MoveNext ()) {
				currentCard = _iterator.Current;
			}
		}

		private void AddCardsToList (int decks, Deck deck, List<Card> cards)
		{

			// Then add decks to the cards list
			for (int i = 0; i < decks; i++) {
				for (int j = 0; j < deck.getCards ().Count; j++) {
					cards.Add (deck.getCards () [j]);
				}
			}
		}
		
	}

}
