﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

namespace Kodaman
{
	public class Hand
	{
		List<Card> cards = new List<Card> ();
		HandHardTotal hard;
		HandSoftTotal soft;
		//HandTotal altTotal;
		private int handValue;
		private int altValue;

		public List<Card> Cards {
			get {
				return cards;
			}
		}

		public int HandValue {
			get {
				return handValue;
			}
		}

		public int AltValue {
			get {
				return altValue;
			}
		}

		public Hand ()
		{
			hard = new HandHardTotal (this);
			soft = new HandSoftTotal (this);
			//altTotal = new HandHardTotal (this);
		}

		public void add (Card card)
		{
			cards.Add (card);
		}

		// Computes the alternate total of this hand using the Hand.altTotal object.
		// If there are any aces, and the soft total is 21 or less, this will be the soft total.
		// If there are no aces, or the soft total is over 21, this will be the hard total.
		public void value ()
		{
			bool hasAce = false;
			int aceCount = 0;
			int aceIndex01 = 0;
			//int aceIndex02 = 0;
			Card tempCard;

			// First we determine if there are any aces. If no aces are found we can use hard total.
			// We also set the indeces of the aces, even though we only need the first one.
			for (int i = 0; i < cards.Count; i++) {
				if (cards [i].getRank () == 1) {
					hasAce = true;
					aceCount++;

					if (aceIndex01 == 0) {
						aceIndex01 = i;
					}

//					if (aceIndex01 != 0) {
//						aceIndex02 = i;
//					}
				}
			}

			// If an ace is found, then calculate soft total, and set altValue to hard
			// If two aces are found use only one as soft (11) the other as hard (1)
			if (hasAce) {

				tempCard = cards [aceIndex01];

				if (aceCount == 1) {
					handValue = soft.total (tempCard);
					handValue += tempCard.softValue ();

					if (handValue >= 21) {
						altValue = hard.total ();
					} 
					if (handValue < 21) {
						altValue = hard.total ();
					} 
//					else {
//						altValue = soft.total ();
//					}
				}

				if (aceCount >= 2) {
					handValue = hard.total (tempCard);
					handValue += tempCard.softValue ();

					//BUG need to fix this statement
					if (handValue >= 21) {
						//handValue = hard.total ();
						altValue = hard.total ();
					} 
					if (handValue < 21) {
						altValue = hard.total ();
					} 
//					else {
//						altValue = soft.total ();
//					}
				}
					
			}

			if (!hasAce) {
				handValue = hard.total ();
			}
				
		}

		public int size ()
		{
			return cards.Count;
		}

		public bool blackjack ()
		{
			if (size () == 2 && handValue == 21) {
				return true;
			}

			return false;
		}

		public bool busted ()
		{
			
			if (handValue > 21 && altValue == 0) {
				return true;
			}

			if (handValue > 21 && altValue <= 21) {
				return false;
			}

			if (handValue > 21 && altValue > 21) {
				return true;
			}

			return false;
		}



		public override string ToString ()
		{
			StringBuilder sbHand = new StringBuilder ();

			foreach (Card card in cards) {
				sbHand.Append (card.ToString ());
			}

			return sbHand.ToString ();
		}
	}
}
