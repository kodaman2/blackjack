﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kodaman;

public class debug : MonoBehaviour
{
	Shoe myShoe;
	public int howManyDecks;
	public int stopDeal;
	public bool testShoe;

	void Start ()
	{
		myShoe = new Shoe (howManyDecks, stopDeal);
		//myShoe.shuffle ();

	}

	public void nextCard ()
	{

		if (!myShoe.isLastShoeCard) {
			//Debug.Log (myShoe.nextCard ().ToString ());
			//myShoe.nextCard ();
			myShoe.nextCardIterator ();
			Debug.LogFormat ("Card rank {0}, and card suit {1}", myShoe.CurrentCard.getRank (), myShoe.CurrentCard.getSuit ());
		} else {
			Debug.LogWarning ("There are no more cards in the shoe, please reset it");
		}
			
	}

	public void printList ()
	{
		foreach (Card card in myShoe.shuffledCards) {
			Debug.Log (card.ToString ());
		}
	}

	public void resetShoe ()
	{
		myShoe.resetShoeAndShuffle ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
}
